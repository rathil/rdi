package rdi

import (
	"reflect"
	"sync"
)

type iProvideOption func(p *provider)

// Get provider option single instance (with cache).
func ProviderOptionSingleInstance() iProvideOption {
	return func(p *provider) {
		p.canCached = true
	}
}

// Get provider option for round robin.
func ProviderOptionRoundRobin() iProvideOption {
	return func(p *provider) {
		p.canRoundRobin = true
	}
}

func newProviderFromOptions(options []iProvideOption) provider {
	p := provider{}
	for _, option := range options {
		option(&p)
	}
	return p
}

func strategyByValue(a provider, pValue reflect.Value) provider {
	a.cache = pValue
	a.invoker = func(a *provider, di *DI) (reflect.Value, error) {
		return a.cache, nil
	}
	return a
}

func strategyByValueRoundRobin(a provider, pValue reflect.Value) provider {
	a.roundRobinIndex = -1
	a.cache = pValue
	a.invoker = func(a *provider, di *DI) (reflect.Value, error) {
		a.mutex.Lock()
		a.roundRobinIndex++
		if a.roundRobinIndex >= a.cache.Len() {
			a.roundRobinIndex = 0
		}
		a.mutex.Unlock()
		return a.cache.Index(a.roundRobinIndex), nil
	}
	return a
}

func strategyByFunctionValue(a provider, function interface{}, index int) provider {
	a.function = function
	a.functionParamIndex = index
	a.invoker = func(a *provider, di *DI) (reflect.Value, error) {
		result, function := a.getCacheOrFunction()
		if !result.IsValid() {
			results, err := di.invoke(function)
			if err != nil {
				return result, err
			}
			result = results[a.functionParamIndex]
			a.setCache(result)
		}
		return result, nil
	}
	return a
}
func strategyByFunctionValueRoundRobin(a provider, function interface{}, index int) provider {
	a.function = function
	a.functionParamIndex = index
	a.roundRobinIndex = -1
	a.invoker = func(a *provider, di *DI) (reflect.Value, error) {
		result, function := a.getCacheOrFunction()
		if !result.IsValid() {
			results, err := di.invoke(function)
			if err != nil {
				return result, err
			}
			result = results[a.functionParamIndex]
			a.setCache(result)
		}
		a.mutex.Lock()
		a.roundRobinIndex++
		if a.roundRobinIndex >= result.Len() {
			a.roundRobinIndex = 0
		}
		a.mutex.Unlock()
		return result.Index(a.roundRobinIndex), nil
	}
	return a
}

type provideMap map[reflect.Type]*provider
type invoker func(*provider, *DI) (reflect.Value, error)

// Struct of one provider
type provider struct {
	canCached          bool
	canRoundRobin      bool
	roundRobinIndex    int
	cache              reflect.Value
	invoker            invoker
	function           interface{}
	functionParamIndex int
	mutex              sync.RWMutex
}

// Get data from cache or function for invoke.
func (a *provider) getCacheOrFunction() (reflect.Value, interface{}) {
	a.mutex.RLock()
	defer a.mutex.RUnlock()
	if !a.canCached {
		return reflect.Value{}, a.function
	}
	return a.cache, a.function
}

// Set data to cache.
func (a *provider) setCache(data reflect.Value) {
	if !a.canCached {
		return
	}
	a.mutex.Lock()
	a.cache = data
	a.function = nil
	a.mutex.Unlock()
}

// Provide data.
func (a *provider) provide(di *DI) (reflect.Value, error) {
	return a.invoker(a, di)
}
