package rdi

import (
	"fmt"
	"log"
	"reflect"
	"sync"
)

// New DI container with Base parent container
func New() *DI {
	return NewWithParent(Base)
}

// NewWithParent - new DI container with self parent container
func NewWithParent(parent *DI) *DI {
	di := &DI{
		provide: provideMap{},
		parent:  parent,
	}
	return di.MustProvide(di)
}

// Base (default) DI container
var Base = NewWithParent(nil)

// DI - struct of DI container
type DI struct {
	provide      provideMap
	parent       *DI
	provideMutex sync.RWMutex
}

// MustProvide is like Provide but panics if method Provide return error.
func (a *DI) MustProvide(value any, options ...iProvideOption) *DI {
	if err := a.Provide(value, options...); err != nil {
		log.Fatal(err)
	}
	return a
}

// Provide - add provider in container or return error if the value can't be represented as provider.
func (a *DI) Provide(provide any, options ...iProvideOption) error {
	pValue := reflect.ValueOf(provide)
	if pValue.Kind() == reflect.Func {
		return a.provideFunction(provide, options)
	}
	if err := a.provideValue(pValue, options); err != nil {
		return err
	}
	return nil
}

// Add function provider in container.
func (a *DI) provideFunction(function any, options []iProvideOption) error {
	vType := reflect.TypeOf(function)
	count := 0
	for i := 0; i < vType.NumOut(); i++ {
		if err := a.provideFunctionValue(function, vType.Out(i), i, options); err != nil {
			return err
		}
		count++
	}
	if count == 0 {
		return fmt.Errorf("can't declare func provider without provide data")
	}
	return nil
}

// Add function value provider in container.
func (a *DI) provideFunctionValue(function any, pType reflect.Type, index int, options []iProvideOption) error {
	if ok, err := a.canAddProvider(pType); err != nil {
		return err
	} else if !ok {
		return nil
	}
	p := newProviderFromOptions(options)
	if err := a.addProvider(
		pType,
		strategyByFunctionValue(p, function, index),
	); err != nil {
		return err
	}
	if p.canRoundRobin {
		if cType, ok := a.getProvideChildType(pType); ok {
			if err := a.addProvider(
				cType,
				strategyByFunctionValueRoundRobin(p, function, index),
			); err != nil {
				return err
			}
		}
	}
	return nil
}

// Add value provider in container.
func (a *DI) provideValue(pValue reflect.Value, options []iProvideOption) error {
	if ok, err := a.canAddProvider(pValue.Type()); err != nil {
		return err
	} else if !ok {
		return fmt.Errorf("can't provide of type \"%s\"", pValue.Type())
	}
	p := newProviderFromOptions(options)
	if err := a.addProvider(
		pValue.Type(),
		strategyByValue(p, pValue),
	); err != nil {
		return err
	}
	if p.canRoundRobin {
		if cType, ok := a.getProvideChildType(pValue.Type()); ok {
			if err := a.addProvider(
				cType,
				strategyByValueRoundRobin(p, pValue),
			); err != nil {
				return err
			}
		}
	}
	return nil
}

// Add provider type to container.
func (a *DI) addProvider(pType reflect.Type, p provider) error {
	a.provideMutex.Lock()
	if _, ok := a.provide[pType]; ok {
		a.provideMutex.Unlock()
		return a.errorCantAddProvider(pType)
	}
	a.provide[pType] = &p
	a.provideMutex.Unlock()
	return nil
}

// Get provider type from container.
func (a *DI) getProvider(pType reflect.Type) (*provider, bool) {
	a.provideMutex.RLock()
	p, ok := a.provide[pType]
	a.provideMutex.RUnlock()
	return p, ok
}

// Check available adding provider.
func (a *DI) canAddProvider(pType reflect.Type) (bool, error) {
	if pType.String() == "error" {
		return false, nil
	}
	if _, ok := a.getProvider(pType); ok {
		return false, a.errorCantAddProvider(pType)
	}
	return true, nil
}

// Create error if not available adding provider.
func (a *DI) errorCantAddProvider(provideType reflect.Type) error {
	return fmt.Errorf("provider of type \"%s\" is already declared", provideType.String())
}

// Get provide child type if provider is Array or Slice.
func (a *DI) getProvideChildType(vType reflect.Type) (reflect.Type, bool) {
	checkType := vType
	if checkType.Kind() == reflect.Ptr {
		checkType = checkType.Elem()
	}
	if checkType.Kind() == reflect.Slice || checkType.Kind() == reflect.Array {
		checkType = checkType.Elem()
		return checkType, true
	}
	return checkType, false
}

// MustInvoke is like Invoke but panics if method Invoke return error.
func (a *DI) MustInvoke(functions ...any) *DI {
	if err := a.Invoke(functions...); err != nil {
		log.Fatal(err)
	}
	return a
}

// Invoke - receive dependencies from container.
func (a *DI) Invoke(functions ...any) error {
	for _, function := range functions {
		if _, err := a.invoke(function); err != nil {
			return err
		}
	}
	return nil
}

// MustInvokePare is like InvokePare but panics if method InvokePare return error.
func (a *DI) MustInvokePare(functionRequest any, functionReceive any) *DI {
	if err := a.InvokePare(functionRequest, functionReceive); err != nil {
		log.Fatal(err)
	}
	return a
}

// InvokePare - receive dependencies from container and send result to second function (functionReceive).
func (a *DI) InvokePare(functionRequest any, functionReceive any) error {
	fValue := reflect.ValueOf(functionReceive)
	if fValue.Type() == nil || fValue.Kind() != reflect.Func {
		return fmt.Errorf("can't invoke not a function second param")
	}
	params, err := a.invoke(functionRequest)
	if err != nil {
		return err
	}
	_, err = a.functionCall(fValue, params)
	return err
}

// Internal receive dependencies from container.
func (a *DI) invoke(function any) ([]reflect.Value, error) {
	var fType reflect.Type
	vType, ok := function.(reflect.Value)
	if ok && vType.IsValid() {
		fType = vType.Type()
	} else {
		fType = reflect.TypeOf(function)
		vType = reflect.ValueOf(function)
	}
	if fType == nil || fType.Kind() != reflect.Func {
		return nil, fmt.Errorf("can't invoke not a function")
	}
	paramsValue := make([]reflect.Value, 0, fType.NumIn())
	for i := 0; i < fType.NumIn(); i++ {
		paramValue, err := a.invokeParam(a, fType.In(i), i)
		if err != nil {
			return nil, err
		}
		paramsValue = append(paramsValue, paramValue)
	}
	return a.functionCall(vType, paramsValue)
}

// Internal call a user function.
func (a *DI) functionCall(fValue reflect.Value, params []reflect.Value) ([]reflect.Value, error) {
	results := fValue.Call(params)
	for _, result := range results {
		if result.Type().String() == "error" {
			if err, ok := result.Interface().(error); ok {
				return nil, err
			}
		}
	}
	return results, nil
}

// Receive one dependence from container.
func (a *DI) invokeParam(di *DI, param reflect.Type, i int) (reflect.Value, error) {
	p, ok := a.getProvider(param)
	if !ok {
		if a.parent != nil {
			return a.parent.invokeParam(di, param, i)
		}
		return reflect.Value{}, fmt.Errorf("not found provider for parameter[%d] of \"%s\" type", i, param.String())
	}
	return p.provide(di)
}
