# GO-lang dependency injection

## Installation

1. Install rDI package:

```sh
$ go get -u gitlab.com/rathil/rdi/v2
```

2. Import it in code:

```go
import "gitlab.com/rathil/rdi/v2"
```

## Quick start

### Custom functionality

```go
type IUser interface{}
type User struct{}

func NewIUser() (IUser, error) {
    return &User{}, nil
}
func NewUserPoint() (*User, error) {
    return &User{}, nil
}
func NewUser() User {
    return User{}
}

type IDevice interface{}
type Device struct{
    User *User
}

var sameErrorDevice = fmt.Errorf(`same device error`)

func NewDevicePoint(user *User, sameVar int) (*Device, error) {
    if sameVar > 5 {
        return nil, sameErrorDevice
    }
    return &Device{user}, nil
}
func NewDevice(user *User) (Device, error) {
    return Device{user}, nil
}
func NewIDevicePoint(user *User) (IDevice, error) {
    return &Device{user}, nil
}
```

### Initialization of the primary container

```go
di := rdi.New()
```

### Provide self as dependency

```go
rdi.New().MustInvoke(func(di *rdi.DI) {
    // ...
})
```

### Provide _(*User, User)_ dependency

```go
user := NewUserPoint()
// ...
if err := di.Provide(user); err != nil {
    panic(err)
}
```
or
```go
if err := di.Provide(NewUserPoint); err != nil {
    panic(err)
}
```
or
```go
di.MustProvide(NewIUser)
di.MustProvide(NewUserPoint)
di.MustProvide(NewUser)
```

### Incorrect provide _IUser_ interface

```go
user := NewIUser()
// ...
di.MustProvide(user) // will be declared type *User
```

### Correct provide _IUser_ interface
```go
di.MustProvide(NewIUser)
```

### Use _(IUser, *User, User)_ dependency

```go
if err := di.Invoke(func(user *User) {
    // ...
}); err != nil {
    panic(err)
}
```
or
```go
var someError = fmt.Errorf(`some error`)
// ...
if err := di.Invoke(func(user *User) error {
    // ...
    if varName > 5 {
        return someError
    }
    return nil
}); err != nil {
    if !errors.Is(err, someError) {
        panic(err)
    }
}
```
or
```go
di.MustInvoke(func(user IUser) {
    // ...
})
di.MustInvoke(func(user *User) {
    // ...
})
di.MustInvoke(func(user User) {
    // ...
})
```

### Available provider options

Each time getting a new context (without options)

```go
di := rdi.New().
    MustProvide(func() context.Context { // It will be called every time when the context is requested
        return context.Background()
    })

di.MustInvoke(func(c context.Context) {
    // ...
})
di.MustInvoke(func(c context.Context) {
    // ...
})
```

Context will be created once and cached (set option **rdi.ProviderOptionSingleInstance()** on providing dependency)

```go
di := rdi.New().
    MustProvide(func() context.Context { // Will be called once
        return context.Background()
    }, rdi.ProviderOptionSingleInstance())

di.MustInvoke(func(c context.Context) {
    // ...
})
di.MustInvoke(func(c context.Context) {
    // ...
})
```

Each time getting **[]int** or **int** will be created new slice (**rdi.ProviderOptionRoundRobin()**)

```go
di := rdi.New().
    MustProvide(func() []int { // It will be called every time when the []int or int is requested
        return []int{1, 2}
    }, rdi.ProviderOptionRoundRobin())

di.MustInvoke(func(data int) { // data == 1
    // ...
})
di.MustInvoke(func(data int) { // data == 2
    // ...
})
di.MustInvoke(func(data int) { // data == 1
    // ...
})
di.MustInvoke(func(data []int) { // data == []int{1, 2}
    // ...
})
```

[]int will be created once and cached and Round-robin by items (**rdi.ProviderOptionSingleInstance()** and **rdi.ProviderOptionRoundRobin()**)

```go
di := rdi.New().
    MustProvide(func() []int { // Will be called once
        return []int{1, 2}
    }, rdi.ProviderOptionSingleInstance(), rdi.ProviderOptionRoundRobin())

di.MustInvoke(func(data int) { // data == 1
    // ...
})
di.MustInvoke(func(data int) { // data == 2
    // ...
})
di.MustInvoke(func(data int) { // data == 1
    // ...
})
di.MustInvoke(func(data []int) { // data == []int{1, 2}
    // ...
})
```

equivalent to the previous example

```go
di := rdi.New().
    MustProvide([]int{1, 2}, rdi.ProviderOptionRoundRobin())
```

### Get dependencies from the container and outside the container

```go
di := rdi.New().
    MustProvide(func() context.Context { // Will be called once
        return context.Background()
    }, rdi.ProviderOptionSingleInstance())

user := NewUser()
if err := di.InvokePare(
    func(c context.Context) (context.Context, User) {
        return c, user
    }
    func(c context.Context, user User) {
        // ...
    }
); err != nil {
    panic(err)
}
```
or
```go
di := rdi.New().
    MustProvide(func() context.Context { // Will be called once
        return context.Background()
    }, rdi.ProviderOptionSingleInstance())

user := NewUser()
di.MustInvokePare(
    func(c context.Context) (context.Context, User) {
        return c, user
    }
    func(c context.Context, user User) {
        // ...
    }
)
```

### Initialize a container copy

A copy is needed to redefine dependencies

```go
di := rdi.New().
    MustProvide(22).
    MustProvide(NewUserPoint)


di.MustInvoke(func(data int) { // data == 22
    // ...
})

diCopy := rdi.NewWithParent(di).
    MustProvide(NewDevicePoint)

diCopy.MustInvoke(func(data int) { // data == 22
    // ...
})
err := diCopy.Invoke(func(device *Device) {
    // ...
})
// err == sameErrorDevice 
// when creating a *Device there is a check on the input int parameter
// if it is more than 5 the device will not be created


diCopyOfCopy := rdi.NewWithParent(diCopy).
    MustProvide(3)

diCopyOfCopy.MustInvoke(func(data int) { // data == 3
    // ...
})
diCopyOfCopy.MustInvoke(func(device *Device) {
    // ...
})
```